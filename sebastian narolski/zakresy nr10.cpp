#include <string>
#include <cstdlib>
#include <algorithm>
#include <iostream>
using namespace std;
int main(int argc,char** srgv){
	int zakres;
	cout<<"podaj zakres liczb do posortowania\n"<<endl;
	cin>>zakres;
	
	cout<<"podaj do posortowania "<<zakres<<" liczb:\n"<<endl;
	int liczby [zakres];
	
	for(int i=0;i<zakres;i++) cin>>liczby[i];
	
	sort(liczby, liczby+zakres);
	
	cout<<"elementy posortowane:"<<endl;
	
	for(int i=0;i<zakres;i++) {
		cout<<liczby[i]<<" ";
	}
	cout<<endl;
	
	return 0;
}
