#include <iostream>
#include <string>
#include <cstdlib>
#include <algorithm>

using namespace std;

int main(int argc,char** argv)
{
   int zakres;
       cout<<"Podaj liczb� zakres�w liczb do posortowania\n"<<endl;
       cin>>zakres;

       cout<<"Podaj do posortowania "<<zakres<<" liczb: \n"<<endl;
       int liczby[zakres];

       for(int i=0;i<zakres;i++) cin>>liczby[i];

       sort(liczby, liczby+zakres);

       cout<<"Elementy posortowane:"<<endl;

       for(int i=0;i<zakres;i++){
        cout<<liczby[i]<<" ";
       }
   cout<<endl;
    return 0;
}
