﻿#include <iostream>
#include <string>
#include <conio.h>
bool sprawdzenie;
using namespace std;
bool Palindrom(string tekst)
{
	int odwrocona = tekst.length() - 1;
	string* kolejnosc1 = new string[tekst.length()];
	string* kolejnosc2 = new string[tekst.length()];
	for (int i = 0; i < tekst.length(); i++)
	{
		string fragment(tekst, i, 1);
		kolejnosc1[i] = fragment;
	}
	for (int j = 0; j < tekst.length(); j++)
	{
		string fragment(tekst, odwrocona, 1);
		kolejnosc2[j] = fragment;
		odwrocona = odwrocona - 1;
	}
	for (int k = 0; k < tekst.length(); k++)
	{
		if (kolejnosc1[k] == kolejnosc2[k])
		{
			::sprawdzenie = true;
		}
		else
		{
			return(false);
		}
	}
	return(true);
}
int main()
{
	string tekst;
	int start = 0;
	bool sprawdz;
	cout << "Podaj tekst: ";
	getline(cin, tekst);
	sprawdz = Palindrom(tekst);
	if (sprawdz == true)
	{
		cout << "Tekst " << tekst << " jest palindromem" << endl;
	}
	else cout << "Tekst " << tekst << " nie jest palindromem" << endl;
	_getch();
}