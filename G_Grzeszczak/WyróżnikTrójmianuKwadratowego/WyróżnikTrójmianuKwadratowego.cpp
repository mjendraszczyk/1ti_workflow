﻿// Wyróżnik Trójmianu Kwadratowego
// Pisałem na Visual Studio

#include <iostream>
#include <conio.h>

using namespace std;
int main()
{
	float a;
	float b;
	float c;
	cout << "Podaj piersza liczbe" << endl;
	cin >> a;
	cout << "Podaj druga liczbe" << endl;
	cin >> b;
	cout << "Podaj trzecia liczbe" << endl;
	cin >> c;

	float wynik = pow(-b, 2) - (4 * a * c);
	cout << "Wartosc wyroznika trojmianu kwadratowego wynosi " << wynik << endl;
	cout << "" << endl;

	if (wynik == 0)
	{
		cout << "Funkcja ma jedno miejsce zerowe" << endl;
	}
	else if (wynik > 0) 
	{
		cout << "Funkcja ma dwa miejsce zerowe" << endl;
	}
	else if (wynik < 0)
	{
		cout << "Funkcja nie ma miejsc zerowych" << endl;
	}
	_getch();
}