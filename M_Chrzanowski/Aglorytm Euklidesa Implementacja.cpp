﻿#include <iostream>
#include <conio.h>
#include <math.h>

using namespace std;
int main()
{
	int liczba1, liczba2;
	int cyfra1, cyfra2;
		do
		{
			cout << endl;
			cout << "Podaj pierwsza liczbe: " << endl;
			cin >> liczba1;
			cout << endl;
			cout << "Podaj druga liczbe: " << endl;
			cin >> liczba2;
			cyfra1 = liczba1;
			cyfra2 = liczba2;
			cout << endl;
			if (liczba1 == 0 || liczba2 == 0)
			{
				cout << "Dzielenie z zerem jest niewykonalne" << endl;
			}else if (liczba1 < 0 || liczba2 < 0)
			{
				cout << "Liczby musza byc wieksze od 0" << endl;
			}
		} while (liczba1 <= 0 || liczba2 <= 0);
	while (liczba1 != liczba2)
	{
		if (liczba1 > liczba2)
		{
			liczba1 = liczba1 - liczba2;
		}
		else if (liczba1 < liczba2)
		{
			liczba2 = liczba2 - liczba1;
		}
		else;
	}
	if (liczba1 != 0 || liczba2 != 0)
	{
		cout << "Najwiekszy wspolny dzielnik liczb " << cyfra1 << " i " << cyfra2 << " to " << liczba1 << endl;
	}
	_getch();
}