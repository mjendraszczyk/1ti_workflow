﻿// KalkulatorBrutto
//Pisałem w Visual Studio

#include <iostream>
#include <conio.h>

using namespace std;
int main()
{
    float cena_netto, podatek, cena_brutto;
    cout << "Podaj cene netto" << endl;
    cin >> cena_netto;
    cout << "Podaj stawke oprocentowania" << endl;
    cin >> podatek;
    cena_brutto = cena_netto * (1 + (podatek / 100));
    cout << "Cena brutto: " << endl;
    cout << cena_netto << " * " << podatek << "%" << " = " << cena_brutto;
    _getch();
}
